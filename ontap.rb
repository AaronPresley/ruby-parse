#!/usr/bin/ruby
# -*- coding: utf-8 -*-

require_relative 'parsers/barparsers'

# The list of bar's we're currently supporting
valid_bars = {
    "apex" => ApexParser,
    "baerlic" => BaerlicParser,
    "imperial" => ImperialParser
}

# Putting our bar strings into a single array
bar_strings = valid_bars.keys.sort

# Get the first argument
requested_bar = ARGV[0]

# Does the user's input match our current bars?
if (!requested_bar) || (!bar_strings.include? requested_bar)

    # Tell the user the err of their ways, IF they entered something
    if requested_bar
        puts ""
        puts "Sorry, \"#{requested_bar}\" isn't a supported bar."
    end

    # List the currently valid bars
    puts ""
    puts "Valid bars are:"

    bar_strings.each do |bar|
        puts "- #{bar}"
    end

    exit
end

# Instantiate the class and output the current taplist
the_class = valid_bars[requested_bar]
parser = the_class.new

# Output the current taplist
puts parser.fetch_beers
