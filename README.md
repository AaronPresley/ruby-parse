# Ruby Parse

This project makes it easy to see what's on tap at my favorite bar around Portland, OR. There are currently 2 base classes: one to make it easy to scrape a Google Spreadsheet, and the other to make it easy to scrape an HTML site. Each bar inherits from one of those.

This is my first project in Ruby. I'm basically porting some of the [Brew Notice](https://brewnotice.com) parsers from Python to learn about Ruby objects.

## Currently Supported Bars

| Bar Name                      | Bar String                | Bar Site
|---                            |---                        |---
| Apex                          | apex                      | http://www.apexbar.com/
| Baerlic Brewing               | baerlic                   | http://baerlicbrewing.com/
| Imperial Bottle Shop          | imperial                  | http://imperialbottleshop.com/

## Usage

To get a bar's current taplist, just run:

    ontap.rb <bar_string>

Here's an example command and output:

    $ ./ontap.rb baerlic
    Baerlic Arctos Nw winter red ale
    Baerlic Birdseye Honey wheat ale
    Baerlic Cask beer friday Rotating
    Baerlic Cavalier Classic cream ale
    Baerlic Dark thoughts Cascadian dark ale
    Baerlic Delight Belgian blond ale
    Baerlic Eastside Oatmeal pilsner
    Baerlic Invincible India pale ale
    Baerlic Noble Oatmeal stout
    Baerlic Old blood & guts 2015 Dryhopped barleywine
    Baerlic Pioneer Oregon special bitter
    Baerlic Primeval Nw brown ale
    Baerlic Spirit bear 50/50 cream ale & stout
    Baerlic Wildcat Farmhouse ale w/ meyer lemon

To see all of the supported bars, don't enter a bar_string argument:

    $ ./ontap.rb

    Valid bars are:
    - apex
    - baerlic
    - imperial
