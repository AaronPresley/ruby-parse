
require 'open-uri'
require 'json'
require 'nokogiri'

class BaseParser
    attr_reader :url

    def initialize(url)
        @url = url
    end

    # Hits the URL and returns the response
    def fetch_response
        # TODO: Some basic error checking of the response or something?
        return open(@url).read
    end

    # Should be implemented by child classes
    def fetch_beers
        raise "Not Implemented"
    end
end

# A generic class for when a bar's taplist is in HTML on their website
class HtmlParser < BaseParser
    attr_reader :url

    def initialize(url)
        super(url)
    end

    # Gets the response and returns a Nokogiri object
    def fetch_page
        response = self.fetch_response
        return Nokogiri::HTML(response)
    end
end

# A generic class for when a bar's taplist is in a publicly
# accessible Google Spreadsheet
class GoogleSpreadsheetParser < BaseParser
    attr_reader :url, :columns

    def initialize(url, columns)
        # Instantiating our base parser
        super(url)

        # The columns, in order, that should be combined
        # into a single string for the beer output
        @columns = columns
    end

    # Parses the response to JSON since we know
    # we're coming from a Google Spreadsheet
    def fetch_response
        # Make the HTTP request
        response = super

        # Parse our response as JSON
        return JSON.parse response
    end

    # Crawls through the @json_response and returns an
    # array of strings that match the @columns
    def fetch_beers
        # Getting the raw response from the URL
        response = self.fetch_response

        # Will hold our list of beer strings
        the_beers = []

        # Looping through each row in the spreadsheet
        response["feed"]["entry"].each do |entry|

            # Will hold this row's output
            beer_output = ""

            # Looping through each field in this row
            entry.each do |field, val|
                # Skip this field if it's not a user-entered field
                if !field.start_with? "gsx$"
                    next
                end

                # Cleaning up the field and value strings
                field = field.gsub(/gsx\$/im, "").strip
                val = val["$t"].strip

                # Loop through each column
                @columns.each do |col|
                    # Append to string if this is the column we want
                    if col == field
                        beer_output << "#{val} "
                    end
                end
            end

            # Cleanup
            beer_output = beer_output.strip

            # Append if the output isn't empty
            if beer_output != ""
                the_beers.push(beer_output)
            end
        end

        return the_beers.sort
    end
end
