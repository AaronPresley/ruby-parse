
require_relative './baseparsers'

# The parser for Imperial Bottle Shop in Portland, OR
class ImperialParser < GoogleSpreadsheetParser
    def initialize
        url = "https://spreadsheets.google.com/feeds/list/1ND6tDPy02os3ZxXEs6xQdEu8hWiBHQ2fyTBHRcNX-PU/1/public/values?alt=json"
        columns = ['brewery', 'beer']
        super(url, columns)
    end
end

# The parser for Apex in Portland, OR
class ApexParser < HtmlParser
    def initialize
        super("http://www.apexbar.com/menu")
    end

    def fetch_beers
        # Getting the response as an object that we scrape
        page = self.fetch_page

        # Will hold our empty list of beers
        the_beers = []

        # Loop through the taplist table
        page.css('table > tbody > tr').each do |row|
            this_beer = row.css('td:nth-child(1)').text
            brewery = row.css('td:nth-child(3)').text
            the_beers.push("#{brewery} #{this_beer}")
        end

        return the_beers.sort
    end
end


# Parser for Baerlic Brewering in Portland, OR
class BaerlicParser < HtmlParser
    def initialize
        super("http://baerlicbrewing.com")
    end

    def fetch_beers
        # Getting our Nokogiri object
        page = self.fetch_page

        # Will hold our list of beers
        the_beers = []

        page.css('.nowOnTap .box').each do |item|
            brewery = "Baerlic"
            beer_name = item.css('h3').text.capitalize
            style = item.css('h4').text.capitalize

            the_beers.push("#{brewery} #{beer_name} #{style}")
        end

        return the_beers.sort

    end
end
